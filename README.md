# PHP-DBCompare

#### 项目介绍
> 找出两个数据库的不同之处 (这里的"不同"是指: src库中有, 但是target库中没有):
- 不同的表
- 相同表中不同的字段
- 相同表中不同的索引
- 相同表中某一字段不同的值


> 在浏览器页面中显示同步用的SQL语句:
- 新建的表
- 新建的字段
- 新建的索引
- 新插入的数据


#### 使用说明
需要安装PHP

```php
require('D:\server\code\PHP-DBCompare\DBCompare.php');

DBCompare::ini()
->setExportTable('^[a-z_0-9]+$') //表名只有 a-z _ 0-9 
->setExportTable('^(?!.*tmp).*') //表名不包含tmp
->setExportTable('table1|table2|table3')
->build('src', 'db-dev.test.com', 'user', 'pwd', 'database', '3306')
->build('target', 'db-pro.test.com', 'user', 'pwd', 'database', '3306')
->diffSchema()
->diffData('table1', 'id')
->diffData('table2', 'age,name')
->outForBrowser();

```
其中: 
- build() 比较'src' 与 'target' 两个数据库的差异
- setExportTable() 指定参与比较的表名, 传入参数是正则表达式. 
    - 例如上边: 表名要是由字母,数字,下划线组成的, 而且不能含有tmp字符串, 而且包含table1或table2或table3字符串; 如果不调用该方法, 表示找到所有表的差异
- diffSchema() 找到src中比target多出来的表, 缺少的字段, 缺少的索引
- diffData() 找到src中某个表某个字段比target多出来的值. 
    - 例如上边: 分别找到两个数据库中, table1中id字段src多出来的值, table2中age+name字段src多出来的值. 注意注意, 数量不要太大
